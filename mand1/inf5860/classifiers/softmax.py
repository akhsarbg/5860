import numpy as np
from random import shuffle

def f(x, W, b):
    return np.dot(W, x) + b

def softmax_loss_naive(W, X, y, reg):
    """
    Softmax loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)
    y_pred = np.zeros_like(y)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using explicit loops.     #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    
    dW = np.dot(X, W) + reg
    
    num_examples = X.shape[0]
    # get unnormalized probabilities
    exp_dW = np.zeros_like(dW)
    for i in range(dW.shape[0]):
        for j in range(dW.shape[1]):
            exp_dW[i, j] = np.exp(dW[i, j])

    # Calcuate sums for each row
    sum = np.zeros((exp_dW.shape[0], 1))
    for i in range(exp_dW.shape[0]):
        for j in range(exp_dW.shape[1]):
            sum[i] +=  exp_dW[i, j]
            
    # normalize them for each example
    probs = np.zeros_like(dW)
    for i in range(exp_dW.shape[0]):
        for j in range(exp_dW.shape[1]):
            probs[i, j] = exp_dW[i,j] / sum[i]
    
    arr = probs[range(num_examples),y]
    corect_logprobs = np.zeros_like(arr)
    for i in range(arr.size):
        corect_logprobs[i] = -np.log(arr[i])
    data_loss = np.sum(corect_logprobs)/num_examples
    
    reg_loss = 0.5*reg*np.sum(W*W)
    loss = data_loss + reg_loss

    dscores = probs
    dscores[range(num_examples),y] -= 1
    dscores /= num_examples
    
    dW = np.dot(X.T, dscores)
    db = np.sum(dscores, axis=0, keepdims=True)
    dW += reg*W # don't forget the regularization gradient
    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################

    return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
    """
    Softmax loss function, vectorized version.

    Inputs and outputs are the same as softmax_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    #############################################################################
    # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
    # Store the loss in loss and the gradient in dW. If you are not careful     #
    # here, it is easy to run into numeric instability. Don't forget the        #
    # regularization!                                                           #
    #############################################################################
    scores = np.dot(X, W) + reg
    scores -= np.max(scores, axis=1, keepdims=True)
    
    num_examples = X.shape[0]
    # get unnormalized probabilities
    exp_scores = np.exp(scores)
    # normalize them for each example
    probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
    
    corect_logprobs = -np.log(probs[range(num_examples),y])
    data_loss = np.sum(corect_logprobs)/num_examples
    reg_loss = 0.5*reg*np.sum(W*W)
    loss = data_loss + reg_loss
    
    dscores = probs
    dscores[range(num_examples),y] -= 1
    dscores /= num_examples
    
    dW = np.dot(X.T, dscores)
    #db = np.sum(dscores, axis=0, keepdims=True)
    dW += reg*W # don't forget the regularization gradient
    #############################################################################
    #                          END OF YOUR CODE                                 #
    #############################################################################
    return loss, dW

