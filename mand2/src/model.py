#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
#                                                                               #
# Part of mandatory assignment 2 in                                             #
# INF5860 - Machine Learning for Image analysis                                 #
# University of Oslo                                                            #
#                                                                               #
#                                                                               #
# Ole-Johan Skrede    olejohas at ifi dot uio dot no                            #
# 2018.03.01                                                                    #
#                                                                               #
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#

"""Define the dense neural network model"""

import numpy as np
from scipy.stats import truncnorm


def one_hot(Y, num_classes):
    """Perform one-hot encoding on input Y.

    It is assumed that Y is a 1D numpy array of length m_b (batch_size) with integer values in
    range [0, num_classes-1]. The encoded matrix Y_tilde will be a [num_classes, m_b] shaped matrix
    with values

                   | 1,  if Y[i] = j
    Y_tilde[i,j] = |
                   | 0,  else
    """
    m = len(Y)
    Y_tilde = np.zeros((num_classes, m))
    Y_tilde[Y, np.arange(m)] = 1
    return Y_tilde


# conf = {'layer_dimensions': [784, 128, 32, 10]}
def initialization(conf):
    """Initialize the parameters of the network.

    Args:
        layer_dimensions: A list of length L+1 with the number of nodes in each layer, including
                          the input layer, all hidden layers, and the output layer.
    Returns:
        params: A dictionary with initialized parameters for all parameters (weights and biases) in
                the network.
    """
    # Done: Task 1
    params = {}
    dims = conf["layer_dimensions"]

    for i in range(len(dims) - 1):
        l = dims[i]
        k = dims[i+1]
        mean = np.zeros((l, k))
        variance = 2./l
        params['b_'+str(i+1)] = np.zeros((k,1))
        params['W_'+str(i+1)] = np.random.normal(mean, np.sqrt(variance))
    
    return params


def activation(Z, activation_function):
    """Compute a non-linear activation function.

    Args:
        Z: numpy array of floats with shape [n, m]
    Returns:
        numpy array of floats with shape [n, m]
    """
    # Done: Task 2 a)
    shape = Z.shape
    Z = Z.reshape(-1)
    A = np.empty_like(Z)
    if activation_function == 'relu':
        for i in range(Z.shape[0]):
                A[i] = Z[i] if Z[i] > 0 else 0.01 * Z[i]
        return A.reshape(shape)        
    else:
        print("Error: Unimplemented derivative of activation function: {}", activation_function)
        return None


def softmax(Z):
    """Compute and return the softmax of the input.

    To improve numerical stability, we do the following

    1: Subtract Z from max(Z) in the exponentials
    2: Take the logarithm of the whole softmax, and then take the exponential of that in the end

    Args:
        Z: numpy array of floats with shape [n, m]
    Returns:
        numpy array of floats with shape [n, m]
    """
    # Done: Task 2 b)
    Z = Z - np.amax(Z, axis=0)
    A = Z - np.log(np.sum(np.exp(Z), axis=0))
    A_out = np.exp(A)

    return A_out


def forward(conf, X_batch, params, is_training):
    """One forward step.

    Args:
        conf: Configuration dictionary.
        X_batch: float numpy array with shape [n^[0], batch_size]. Input image batch.
        params: python dict with weight and bias parameters for each layer.
        is_training: Boolean to indicate if we are training or not. This function can namely be
                     used for inference only, in which case we do not need to store the features
                     values.

    Returns:
        Y_proposed: float numpy array with shape [n^[L], batch_size]. The output predictions of the
                    network, where n^[L] is the number of prediction classes. For each input i in
                    the batch, Y_proposed[c, i] gives the probability that input i belongs to class
                    c.
        features: Dictionary with
                - the linear combinations Z^[l] = W^[l]a^[l-1] + b^[l] for l in [1, L].
                - the activations A^[l] = activation(Z^[l]) for l in [1, L].
               We cache them in order to use them when computing gradients in the backpropagation.
    """
    # Done: Task 2 c)
    Y_proposed = None
    #Add input layer at starting pos
    features = {'A_0': X_batch}
    activation_function = conf['activation_function']


    length = len(params)//2+1
    for i in range(1, length):
        idx = str(i)
        W  = np.dot(params['W_'+idx].T,  features['A_'+str(i-1)])
        features['Z_'+idx] = W + params['b_'+idx ]
        features['A_'+idx] = activation(features['Z_'+idx], activation_function)
        
    Y_proposed = softmax( features['Z_'+str(length-1)] )
    features = features if is_training == True else None

    return Y_proposed, features


def cross_entropy_cost(Y_proposed, Y_reference):
    """Compute the cross entropy cost function.

    Args:
        Y_proposed: numpy array of floats with shape [n_y, m].
        Y_reference: numpy array of floats with shape [n_y, m]. Collection of one-hot encoded
                     true input labels

    Returns:
        cost: Scalar float: 1/m * sum_i^m sum_j^n y_reference_ij log y_proposed_ij
        num_correct: Scalar integer
    """
    # Done: Task 3
    cost = 0
    num_correct = 0
    [n, m] = Y_proposed.shape
    cost = np.sum(-1.0/m * Y_reference * np.log(Y_proposed))

    Y_proposed_max  = np.argmax(Y_proposed, axis=0)
    Y_reference_max = np.argmax(Y_reference, axis=0)
    num_correct = np.sum(Y_proposed_max  == Y_reference_max)

    return cost, num_correct


def activation_derivative(Z, activation_function):
    """Compute the gradient of the activation function.

    Args:
        Z: numpy array of floats with shape [n, m]
    Returns:
        numpy array of floats with shape [n, m]
    """
    # Done: Task 4 a)
    if activation_function == 'relu':
        return (Z >= 0)
    else:
        print("Error: Unimplemented derivative of activation function: {}", activation_function)
        return None


def backward(conf, Y_proposed, Y_reference, params, features):
    """Update parameters using backpropagation algorithm.

    Args:
        conf: Configuration dictionary.
        Y_proposed: numpy array of floats with shape [n_y, m].
        features: Dictionary with matrices from the forward propagation. Contains
                - the linear combinations Z^[l] = W^[l]a^[l-1] + b^[l] for l in [1, L].
                - the activations A^[l] = activation(Z^[l]) for l in [1, L].
        params: Dictionary with values of the trainable parameters.
                - the weights W^[l] for l in [1, L].
                - the biases b^[l] for l in [1, L].
    Returns:
        grad_params: Dictionary with matrices that is to be used in the parameter update. Contains
                - the gradient of the weights, grad_W^[l] for l in [1, L].
                - the gradient of the biases grad_b^[l] for l in [1, L].
    """
    # Done: Task 4 b)
    [n, m] = Y_proposed.shape
    grad_params = {}
    activation_function = conf['activation_function']
    J = Y_proposed - Y_reference

    length = len(conf['layer_dimensions'])
    for i in reversed(range(1, length)):
        grad_params['grad_W_'+str(i)] = 1.0/m * np.dot(features['A_'+str(i-1)], J.T )
        grad_params['grad_b_'+str(i)] = 1.0/m * np.dot(J, np.ones((m,1)))

        if i == 1:
            break
        else:
            gZ = activation_derivative(features['Z_'+str(i-1)], activation_function)
            WJ = np.dot(params['W_'+str(i)], J)        
            J = np.multiply(gZ, WJ)

    return grad_params


def gradient_descent_update(conf, params, grad_params):
    """Update the parameters in params according to the gradient descent update routine.

    Args:
        conf: Configuration dictionary
        params: Parameter dictionary with W and b for all layers
        grad_params: Parameter dictionary with b gradients, and W gradients for all
                     layers.
    Returns:
        updated_params: Updated parameter dictionary.
    """
    # Done: Task 5
    updated_params = {}
    delta = conf['learning_rate']

    for i in range(1, len(params) // 2 + 1):
        updated_params['W_'+str(i)] = params['W_'+str(i)] - delta * grad_params['grad_W_'+str(i)]
        updated_params['b_'+str(i)] = params['b_'+str(i)] - delta * grad_params['grad_b_'+str(i)]

    
    return updated_params
